#!/bin/bash

if [ -z ${EIRDBASE+x} ]
then
  EIRDBASE=../../Database
fi

ln -s input/1d.input-H_slab fort.1
ln -s input/block45-H .

ln -s $EIRDBASE/AMdata/amjuel.tex AMJUEL
ln -s $EIRDBASE/AMdata/hydhel.tex HYDHEL
ln -s $EIRDBASE/Surfacedata/SPUTER .
ln -s $EIRDBASE/Surfacedata/TRIM .

mkdir -p output
ln -s output/H_slab.out .
ln -s output/H_slab.err .
ln -s output/output.0000 .
ln -s output/output.0001 .
ln -s output/output.0002 .
ln -s output/output.0003 .
ln -s output/outfarm.0000 .
ln -s output/outfarm.0001 .
ln -s output/outfarm.0002 .
ln -s output/outfarm.0003 .
ln -s output/fort.7 .
ln -s output/fort.10 .
ln -s output/fort.11 .
ln -s output/fort.12 .
ln -s output/fort.13 .
ln -s output/fort.55 .
