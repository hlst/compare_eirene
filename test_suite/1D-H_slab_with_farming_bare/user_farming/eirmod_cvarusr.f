      module eirmod_cvarusr

      USE EIRMOD_PRECISION
      USE EIRMOD_PARMMOD

      IMPLICIT NONE

      private

      public :: eirene_alloc_cvarusr, eirene_dealloc_cvarusr,
     .          eirene_init_cvarusr

      integer, parameter, public :: mxparams = 36

      real(dp), public, save :: 
     .                  tepars(12),tepare(12),tepard(12),tescl(12),
     .                  tipars(12),tipare(12),tipard(12),tiscl(12),
     .                  dnipars(12),dnipare(12),dnipard(12),dniscl(12),
     .                  pp(12), perm(mxparams), scl(mxparams)

      integer, public, save :: ivaried(mxparams)

      real(dp), allocatable, public, save :: parm_mat(:,:)

      integer, public, save :: nobs = mxparams+1
      real(dp), allocatable, public, save :: store_xobs(:,:)

      integer, public, save :: npte(12), npti(12), npdni(12), 
     .                         npvar(mxparams)
      integer, public, save :: mxvar, nperms_done, nvaried,
     .                         modusr_call=0

      contains

      subroutine eirene_alloc_cvarusr (ical)

      implicit none
      integer, intent(in) :: ical
      
      if (ical == 2) then
        if (allocated(store_xobs)) then
          if (size(store_xobs,2) /= nobs) then
            deallocate (store_xobs)
          end if
        end if

        if (.not.allocated(store_xobs)) then
           allocate (store_xobs(nrounds,nobs))
        end if
      end if

      call eirene_init_cvarusr(ical)

      return
      end subroutine eirene_alloc_cvarusr



      subroutine eirene_dealloc_cvarusr

      implicit none

      if (allocated(store_xobs)) deallocate (store_xobs)

      return
      end subroutine eirene_dealloc_cvarusr


      subroutine eirene_init_cvarusr (ical)

      implicit none
      integer, intent(in) :: ical

      if (ical == 1) then
        tepars = 0._dp
        tepare = 0._dp
        tepard = 0._dp
        tescl = 1._dp
        tipars = 0._dp
        tipare = 0._dp
        tipard = 0._dp
        tiscl = 1._dp
        dnipars = 0._dp
        dnipare = 0._dp
        dnipard = 0._dp
        dniscl = 1._dp
        
        npte = 1
        npti = 1
        npdni = 1
        
        npvar = 1
        
        nperms_done = 0

      else if (ical == 2) then

        store_xobs = 0._dp

      end if

      return
      end subroutine eirene_init_cvarusr


      end module eirmod_cvarusr
