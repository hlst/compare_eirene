c
c
      subroutine EIRENE_modusr

! execute parameter variations

! the parameter variations can be seen as permutations of the given parameter set

! if considering n parameters with k variations per parameter one can
! interpret each permutation as an n digit number in a k based counting system
! thus it is possible to cover the whole permutation range by starting with
! 000...000 and adding 1_k successive for each permutation.

! if your problem has n parameters with k(i),i=1,n variations you can
! do similarly but the counting system is slightly more complicated

! n parameters with k variations lead to k**n different permutations.
  
! distribution of the permutations on NPRS processors is done in the following way
! in the first entry into EIRENE_MODUSR each processor converts its rank (number)
! into a permutation.
! according to this permutation it finds the profile parameters from the
! parameter matrix.
! in all following entries into EIRENE_MODUSR it adds NPRS to the
! permutation it calculated the last time.
! thus given NPRS=4 processor 0 calculates permutations 0,4,8,...
! processor 1 calculates permutations 1,5,9,...
   
      use eirmod_precision
      use eirmod_parmmod
      use eirmod_cvarusr
      use eirmod_cpes
      use eirmod_comusr 
      use eirmod_comsou 
      use eirmod_comprt 
      use eirmod_cinit 
      use eirmod_cgrid
      
      implicit none

      REAL(DP), ALLOCATABLE, save :: HELP(:)
      integer, save :: ifirst=0, icall, iperm(36), iadd(36)
      integer :: i, ier

      if (ifirst == 0) then
        iperm = 0
! initialize permutation with number of processor
        call eirene_conv_int_perm (iperm,my_pe)
! calculate permutation representing total number of processors
        iadd = 0
        if (nfarms > nprs) call eirene_conv_int_perm (iadd,nprs)
        allocate (help(nrad))
        ifirst = 1
        icall = 0
        nperms_done = 0
        if (nprs > nfarms) procforstra(1:nstrai,nfarms:nprs-1)=.false.
      else 
! in each call increase permutation by number of processors
        call eirene_add_perms (iperm, iadd, ier)
        if (ier /= 0) procforstra(1:nstrai,my_pe) = .false.
      end if

      do i=1,mxparams
        perm(i) = parm_mat(iperm(i),i)
      end do

      modusr_call = 1
      icall = icall + 1
      write (iunout,*)
      write (iunout,*) icall,'. call to modusr '
      write (iunout,*)

! Te
      pp(1:12) = perm(1:12)*scl(1:12)
      CALL EIRENE_PROUSR (TEIN,0,TE0,TE1,TE2,TE3,TE4,TE5,TVAC,NSURF)
      
! Ti = Te, except for a multiplier  
!pb      pp(1:11) = perm(1:11)*scl(1:11)
      pp(1:11) = perm(13:23)*scl(13:23)
      pp(12) = perm(24)*scl(24)
      CALL EIRENE_PROUSR (HELP,1+0*NPLS,TI0(1),TI1(1),TI2(1),
     .                    TI3(1),TI4(1),TI5(1),TVAC,NSURF)
      TIIN(1,1:NSURF)=HELP(1:NSURF)

! Ni
      pp(1:12) = perm(25:36)*scl(25:36)
      CALL EIRENE_PROUSR (HELP,1+1*NPLS,DI0(1),DI1(1),DI2(1),
     .                    DI3(1),DI4(1),DI5(1),DVAC,NSURF)
      DIIN(1,1:NSURF)=HELP(1:NSURF)


C
C  COMPUTE SOME 'DERIVED' PLASMA DATA PROFILES FROM THE INPUT PROFILES
C
      call eirene_plasma_deriv(0)
C
C  SET ATOMIC DATA TABLES
C
      CALL EIRENE_SETAMD(1)

C  ATOMIC & MOLECULAR DATA DIAGNOSTICS ON ADDITIONAL INPUT ARRAY ADIN
C
      call eirene_amdiag
C
C  PRINT VOLUME AVERAGED INPUT TALLIES.
C
      CALL EIRENE_OUTPLA(0)

      return

      contains

      subroutine eirene_conv_int_perm (iar,num)
    
      integer, intent(inout) :: iar(36)
      integer, intent(in) :: num
      integer :: i, iquot

      iar = 0
      if (num == 0) return

      iquot = num

      do i=1,36
        if (npvar(i) > 1) then
          iar(i) = mod(iquot,npvar(i))
          iquot = iquot / npvar(i)
        endif
        if (iquot == 0) exit
      end do

!      if (i > 36) then
!         write (iunout,*) ' ERROR in CONV_INT_PERM '
!         write (iunout,*) ' can not convert ',num
!         call eirene_exit_own(1)
!      end if
        
      return
      end subroutine eirene_conv_int_perm


      subroutine eirene_add_perms (iperm,iadd,ier)
      integer, intent(inout) :: iperm(36), ier
      integer, intent(in) :: iadd(36) 
      integer, save :: iueber(37)
      integer :: i, isu

      iueber = 0
      ier = 0
      
      do i=1,36
        isu = iperm(i) + iadd(i) + iueber(i)
        iueber(i+1) = isu / npvar(i)
        iperm(i) = mod(isu,npvar(i))
      end do

      if (iueber(37) /= 0) then
        ier = 1
      end if

      return
      end subroutine eirene_add_perms

      end subroutine eirene_modusr
