

      SUBROUTINE EIRENE_COLLECT_DATA_USR

      USE EIRMOD_PRECISION
      USE EIRMOD_PARMMOD
      USE EIRMOD_CPES
      USE EIRMOD_CVARUSR

      IMPLICIT NONE
      include 'mpif.h'
      integer :: ier, ibuf, iout, ip, j
      character(20) :: fname

      fname = 'outfarm.            '
      write (fname(9:),'(i0.4)') my_pe
      open (newunit=iout,file=trim(fname),access='SEQUENTIAL',
     .      form='FORMATTED')

      write (iout,'(A,i6)') 'nperms_done ',nperms_done, 'nobs ', nobs,
     .                        'nvaried ',nvaried
      write (iout,*) 'varied parameters '
      write (iout,'(10i6)') ivaried

      do ip = 1, nperms_done
        write (iout,'(i6,10es12.4,/(6x,10es12.4))')
     .       ip, (store_xobs(ip,j),j=1,nobs)
      end do

      close (iout)

      if (nprs > 1) then
        call mpi_barrier(mpi_comm_world,ier)

        call mpi_reduce(nperms_done,ibuf,1,
     .       mpi_integer,mpi_sum,0,mpi_comm_world,ier)
        if (my_pe == 0) nperms_done=ibuf
      end if

      RETURN
      END
