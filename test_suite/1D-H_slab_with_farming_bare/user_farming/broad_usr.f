

      SUBROUTINE EIRENE_BROAD_USR

      use eirmod_precision
      use eirmod_parmmod
      use eirmod_cpes
      use eirmod_cvarusr

      IMPLICIT NONE
      integer :: ier

      INCLUDE 'mpif.h'

      CALL MPI_BARRIER(MPI_COMM_WORLD,ier)
       
      CALL MPI_BCAST (MXVAR,1,MPI_INTEGER,0,MPI_COMM_WORLD,ier)
      CALL MPI_BCAST (NOBS,1,MPI_INTEGER,0,MPI_COMM_WORLD,ier)

      if (my_pe /= 0) call eirene_alloc_cvarusr(1)
      
      if (.not.allocated(parm_mat)) allocate (parm_mat(0:mxvar-1,36))

      CALL MPI_BCAST (PARM_MAT,mxparams*mxvar,MPI_REAL8,0,
     .                MPI_COMM_WORLD,ier)
      CALL MPI_BCAST (SCL,MXPARAMS,MPI_REAL8,0,MPI_COMM_WORLD,ier)
      CALL MPI_BCAST (NPVAR,MXPARAMS,MPI_INTEGER,0,MPI_COMM_WORLD,ier)
       
      CALL MPI_BCAST (nvaried,1,MPI_INTEGER,0,MPI_COMM_WORLD,ier)
      CALL MPI_BCAST (ivaried,mxparams,MPI_INTEGER,0,MPI_COMM_WORLD,ier)
      
      RETURN
      END
