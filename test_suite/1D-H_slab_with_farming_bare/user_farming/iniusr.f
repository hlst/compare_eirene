 
 
      SUBROUTINE EIRENE_INIUSR

      use eirmod_precision
      use eirmod_parmmod
      USE EIRMOD_COMPRT
      use eirmod_cestim
      use eirmod_cvarusr

      IMPLICIT NONE

      integer :: i, j , nf, mxbin=0
      character(80) :: zeile

      if ((nadspc > 0).and.allocated(estiml)) then
         mxbin = 0
         do i = 1, nadspc
            mxbin = max(mxbin, estiml(i)%nspc)
         end do
         nobs = nobs + mxbin+2
      end if

      CALL EIRENE_ALLOC_CVARUSR(1)

!      READ (IUNIN,*)
      READ (IUNIN,'(A72)') ZEILE
      DO WHILE (ZEILE(1:3) .NE. '***')
        READ (IUNIN,'(A72)') ZEILE
      END DO

! Te
      READ (IUNIN,'(A80)') ZEILE  !  DENSITY, TEMPERATURE, ETC... PROFILE 

      read (IUNIN,'(6E12.4)') (TEPARS(I),I=1,6)
      read (IUNIN,'(6E12.4)') (TEPARS(I),I=7,12)

      read (IUNIN,'(6E12.4)') (TEPARE(I),I=1,6)
      read (IUNIN,'(6E12.4)') (TEPARE(I),I=7,12)

      read (IUNIN,'(6E12.4)') (TESCL(I),I=1,6)
      read (IUNIN,'(6E12.4)') (TESCL(I),I=7,12)

      read (iunin,'(12i6)') (npte(i),i=1,12)

! Ti
      READ (IUNIN,'(A80)') ZEILE  !  DENSITY, TEMPERATURE, ETC... PROFILE 

      read (IUNIN,'(6E12.4)') (TIPARS(I),I=1,6)
      read (IUNIN,'(6E12.4)') (TIPARS(I),I=7,12)

      read (IUNIN,'(6E12.4)') (TIPARE(I),I=1,6)
      read (IUNIN,'(6E12.4)') (TIPARE(I),I=7,12)

      read (IUNIN,'(6E12.4)') (TISCL(I),I=1,6)
      read (IUNIN,'(6E12.4)') (TISCL(I),I=7,12)

      read (iunin,'(12i6)') (npti(i),i=1,12)

! Ni
      READ (IUNIN,'(A80)') ZEILE  !  DENSITY, TEMPERATURE, ETC... PROFILE 

      read (IUNIN,'(6E12.4)') (DNIPARS(I),I=1,6)
      read (IUNIN,'(6E12.4)') (DNIPARS(I),I=7,12)

      read (IUNIN,'(6E12.4)') (DNIPARE(I),I=1,6)
      read (IUNIN,'(6E12.4)') (DNIPARE(I),I=7,12)

      read (IUNIN,'(6E12.4)') (DNISCL(I),I=1,6)
      read (IUNIN,'(6E12.4)') (DNISCL(I),I=7,12)

      read (iunin,'(12i6)') (npdni(i),i=1,12)

! calculate number of different parameter variations
      nf = product(npte) * product(npti) * product(npdni)

      if (nfarms == 1) then
         nfarms = nf
      else if (nf /= nfarms) then
         write (iunout,*) ' PROBLEM IN INIUSR '
         write (iunout,*) ' NF.NE.NFARMS ',NF, NFARMS
         call eirene_exit_own(1)
      end if   

! set up deltas for parameter variations
      do i=1,12

        if (npte(i) > 1) tepard(i) = (tepare(i)-tepars(i))/(npte(i)-1)
        if (npti(i) > 1) tipard(i) = (tipare(i)-tipars(i))/(npti(i)-1)
        if (npdni(i) > 1) 
     .        dnipard(i) = (dnipare(i)-dnipars(i))/(npdni(i)-1)

      end do

      mxvar = max(maxval(npte),maxval(npti),maxval(npdni))

! calculate matrix of parameter variations
      if (.not.allocated(parm_mat)) allocate (parm_mat(0:mxvar-1,36))

      parm_mat = 0._dp
      
      parm_mat(0,1:12) = tepars(1:12)
      parm_mat(0,13:24) = tipars(1:12)
      parm_mat(0,25:36) = dnipars(1:12)

      scl(1:12) = tescl(1:12)
      scl(13:24) = tiscl(1:12)
      scl(25:36) = dniscl(1:12)

      npvar(1:12) = npte
      npvar(13:24) = npti
      npvar(25:36) = npdni

      nvaried = 0
      do i = 1, 36
         if (npvar(i) > 1) then
            nvaried = nvaried + 1
            ivaried(i) = 1
         end if
      end do

      do i=1,12
! Te
        do j= 2, npte(i)
          parm_mat(j-1,i) = parm_mat(0,i) + (j-1)*tepard(i)
        end do
        parm_mat(npte(i)-1:mxvar-1,i) = tepare(i)
! Ti
        do j= 2, npti(i)
          parm_mat(j-1,12+i) = parm_mat(0,12+i) + (j-1)*tipard(i)
        end do
        parm_mat(npti(i)-1:mxvar-1,12+i) = tipare(i)
! Ni
        do j= 2, npdni(i)
          parm_mat(j-1,24+i) = parm_mat(0,24+i) + (j-1)*dnipard(i)
        end do
        parm_mat(npdni(i)-1:mxvar-1,24+i) = dnipare(i)
      end do

      RETURN
      END
