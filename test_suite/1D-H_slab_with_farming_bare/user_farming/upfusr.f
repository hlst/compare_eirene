      subroutine eirene_upfusr

      use eirmod_precision
      use eirmod_parmmod
      use eirmod_cvarusr
      use eirmod_coutau
      use eirmod_cestim
      use EIRMOD_COMPRT, ONLY: IUNOUT
      use eirmod_cpes

      implicit none

      real(dp), allocatable, save :: xob(:)
      integer, save :: ifirst=0
      integer :: i, j, ndp, nsps
      character(12) :: ch(nobs)

      if (ifirst == 0) then
         ifirst = 1
         allocate(xob(nobs))
         xob = 0._dp
         call eirene_alloc_cvarusr(2)
         store_xobs = 0._dp
      end if

      if (any(procforstra(:,my_pe))) then
        ndp = size(perm)
        xob(1:ndp) = perm
        xob(ndp+1) = sptatti(1)
        if (nadspc > 0) then
           nsps = estiml(1)%nspc + 2
           xob(ndp+1+1:ndp+1+nsps) = estiml(1)%spc(0:nsps-1)
        end if
        nperms_done = nperms_done + 1

        store_xobs(nperms_done,1:nobs) = xob(1:nobs)
      end if

      end subroutine eirene_upfusr
