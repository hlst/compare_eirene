CDK USER
C
C   USER SUPPLIED SUBROUTINES
C
C           *******************
C           *  Mitja Beckers  *  (system code)
C           *******************
C
      SUBROUTINE EIRENE_PROUSR (PRO,INDX,P0,P1,P2,P3,P4,P5,PROVAC,N)

C   radial grid size:  N, i.e. N-1 cells.
C   SET RADIAL PROFILE PRO, PRO(I=1,N-1) WITH PRO(N-1)=PROVAC
C   P0,P5: NOT IN USE
C   READ PP1,...PP12,  PROFILE PARAMETERS

C  INDX = 1+1*NPLS:  DIIN
C
C     PP1 : CENTRAL VALUE             
C     PP2 : RADIAL POSITION OF TOP OF PEDESTAL, NORMALIZED TO RHO_SEP=1
C     PP3 : CORE PROFILE SHAPE PARAMETER
C     PP4 : RADIAL POSITION OF SEPARATRIX (CM), CORRESP. TO RHO=1                     
C     PP5 : VALUE AT TOP OF PEDESTAL
C     PP6 : VALUE AT SEPARATIX (=BOTTOM OF PEDESTAL), CORRESP. TO RHO=1                    
C     PP7 : 
C     PP8 : 
C     PP9 :                      
C     PP10 : DECAY LENGTH IN SOL (CM)
C     PP11 : MINIMUM IN FAR SOL
C     PP12 : MULTIPLICATIVE FACTOR FOR 
      USE EIRMOD_PRECISION
      USE EIRMOD_PARMMOD
      USE EIRMOD_COMUSR
      USE EIRMOD_COMPRT
      USE EIRMOD_CGRID
      USE EIRMOD_CGEOM
      USE EIRMOD_CVARUSR
      IMPLICIT NONE
      REAL(DP), INTENT(IN) :: P0, P1, P2, P3, P4, P5, PROVAC
      REAL(DP), INTENT(OUT) :: PRO(*)
      INTEGER, INTENT(IN) :: INDX, N

      REAL(DP) :: RHO, SLOPE, RHOTOP,PRO0,DELR
      CHARACTER(80) :: ZEILE
      INTEGER IFIRST,I
      data ifirst /0/
      
      if (modusr_call == 0) then
        if (indx == 0) then
          pp(1:12) = tepars(1:12)*tescl(1:12)
        elseif (indx == 1) then
          pp(1:12) = tipars(1:12)*tiscl(1:12)
        elseif (indx == 1+npls) then
          pp(1:12) = dnipars(1:12)*dniscl(1:12)
        end if
      end if

      WRITE (IUNOUT,*) 'p1: central value (on magn. axis)       ',PP(1)
      WRITE (IUNOUT,*) 'p2: rho_pedestal, between 0 and 1       ',PP(2)
      WRITE (IUNOUT,*) 'p3: core profile shape parameter        ',PP(3)
      WRITE (IUNOUT,*) 'p4: minor radius (cm), LCFS (rho=1)     ',PP(4)
      WRITE (IUNOUT,*) 'p5: value at pedestal top               ',PP(5)
      WRITE (IUNOUT,*) 'p6: value at pedestal bottom (at rho=1) ',PP(6)

      WRITE (IUNOUT,*) 'p7,p8,p9: currently not in use         '
      WRITE (IUNOUT,*) 'p10: exp. decay length at rho .ge. 1    ',PP(10)
      WRITE (IUNOUT,*) 'p11: ceiling (minimum) or profile in SOL',PP(11)
      WRITE (IUNOUT,*) 'p12: multiplicative factor for profile  ',PP(12)

      RHOTOP=PP(5)

cdr   WRITE (IUNOUT,*) 'RHOTOP,   ',RHOTOP

      DO I=1,N-1
cvac  in case one vacuum zone is set (rra .gt.raa), this must be I=1,N-2
        RHO=RHOZNE(I)/PP(4)  !   turn radial grid point into relative "rho" grid

        IF (RHO.LE.PP(2)) THEN         
C  CORE PROFILE
          PRO(I)= PP(5)+(PP(1)-PP(5))*(1.0-(RHO/PP(2))**2)**PP(3)
        ELSEIF (RHO.GT.PP(2).AND.RHO.LE.1.0) THEN
C  PEDESTAL PROFILE, LINEAR DECAY FROM TOP OF PEDESTAL TO SEPARATRIX     
          SLOPE= (RHOTOP-PP(6))/(PP(2)-1.)  ! NEGATIVE SLOPE, separatrix corresponds to rho=1.
          PRO(I)= RHOTOP+SLOPE*(RHO-PP(2))           
        ELSEIF (RHO.GT.1.0) THEN  
C  SOL PROFILE
          PRO0= PP(6) ! VALUE AT RHO=1 (SEPARATRIX), FROM HERE: EXPONENTIAL DECAY, UNTIL CEILING
          DELR=RHOZNE(I)-PP(4)  ! DISTRANCE INTO SOL, CM
          PRO(I)=MAX(PP(11),PRO0*EXP(-DELR/PP(10)))
        ENDIF
      ENDDO
C  MULTIPLICATIVE FACTOR
      DO I=1,N-1
cvac  in case one vacuum zone is set (rra .gt.raa), this must be I=1,N-2
        PRO(I)=PP(12)*PRO(I)
      ENDDO
cvac  PRO(N-1)=PROVAC
 
      RETURN
      END
