C
C
      SUBROUTINE EIRENE_OUTUSR
      USE EIRMOD_PRECISION
      USE EIRMOD_PARMMOD
      use EIRMOD_COMPRT, ONLY: IUNOUT
      use eirmod_cvarusr

      IMPLICIT NONE

      integer :: i, j
      real(dp) :: xnm1
      character(12) :: ch(nobs)

      call eirene_leer(2)

      write (iunout,'(a)') 'FARMING SUMMARIZATION'
      write (iunout,'(A,i6)') 'no. of permutations requested ',nfarms
      write (iunout,'(A,i6)') 'no. of permutations done      ',
     .                         nperms_done
      write (iunout,'(A,i6)') 'no. of parameters             ',mxparams      
      write (iunout,'(A,i6)') 'no. of parameters of varied   ',nvaried
      write (iunout,*) 'varied parameters '
      write (iunout,'(10i6)') ivaried    

      RETURN
      END
