#!/usr/bin/bash

if [[ $1 -gt 1 ]]
then
    THREADS=$1
else
    THREADS=1
fi

THREADS=4

echo "Running cylinder case: comparing $THREADS threads with the serial version"

rm -f eirene
make > /dev/null

export OMP_NUM_THREADS=1
rm -f fort.*
./set_links.sh > /dev/null 2>&1
ln -sf input_nptsdelx$THREADS.dat input/input.eir
echo "Running EIRENE with $OMP_NUM_THREADS thread"
./eirene > stdout.nptsdelx$THREADS
rm -rf output.nptsdelx$THREADS
mv output output.nptsdelx$THREADS
tail -1 stdout.nptsdelx$THREADS

export OMP_NUM_THREADS=$THREADS
rm -f fort.*
./set_links.sh > /dev/null 2>&1
ln -sf input_nptsdelx$THREADS.dat input/input.eir
echo "Running EIRENE with $OMP_NUM_THREADS threads"
./eirene > stdout.$THREADS
rm -rf output.$THREADS
mv output output.$THREADS
tail -1 stdout.$THREADS

./compare_eirene.py output.nptsdelx$THREADS output.$THREADS

#echo "Output in stdout.nptsdelx$THREADS and dbg.$THREADS"
