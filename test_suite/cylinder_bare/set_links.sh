#!/bin/bash

if [ -z ${EIRDBASE+x} ]
then
  if [ -z ${EIRDIR+x} ]
  then
    EIRDIR=../..
  fi
  EIRDBASE=$EIRDIR/Database
fi

ln -s input/input.eir fort.1

ln -s $EIRDBASE/AMdata/amjuel.tex AMJUEL
ln -s $EIRDBASE/AMdata/hydhel.tex HYDHEL
ln -s $EIRDBASE/Surfacedata/SPUTER .
ln -s $EIRDBASE/Surfacedata/TRIM .
ln -s $EIRDBASE/Surfacedata/TRIM/D_on_Be .
ln -s $EIRDBASE/Surfacedata/TRIM/D_on_Fe .
ln -s $EIRDBASE/Surfacedata/TRIM/Be_on_Be .

mkdir -p output
ln -s output/cylinder.out .
ln -s output/cylinder.err .
ln -s output/fort.10 .
ln -s output/fort.11 .
ln -s output/fort.12 .
ln -s output/fort.13 .
ln -s output/fort.55 .
