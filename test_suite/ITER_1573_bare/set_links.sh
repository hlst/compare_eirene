#!/bin/bash

if [ -z ${EIRDBASE+x} ]
then
  if [ -z ${EIRDIR+x} ]
  then
    EIRDIR=../..
  fi
  EIRDBASE=$EIRDIR/Database
fi

ln -s input/input.dat fort.1
ln -s input/iter.linda_geometry fort.30
ln -s input/iter.plasma fort.31
ln -s input/iter.npco_char fort.33
ln -s input/iter.elemente fort.34
ln -s input/iter.neighbors fort.35

ln -s $EIRDBASE/AMdata/amjuel.tex AMJUEL
ln -s $EIRDBASE/AMdata/hydhel.tex HYDHEL
ln -s $EIRDBASE/AMdata/methane.tex METHANE
ln -s $EIRDBASE/AMdata/h2vibr.tex H2VIBR
ln -s $EIRDBASE/Surfacedata/SPUTER .
ln -s $EIRDBASE/Surfacedata/TRIM/D_on_C .
ln -s $EIRDBASE/Surfacedata/TRIM/He_on_C .

mkdir -p output
ln -s output/out_mod.gf .
ln -s output/err_mod.gf .
ln -s output/fort.10 .
ln -s output/fort.11 .
ln -s output/fort.12 .
cp input/fort.13 output
ln -s output/fort.13 .
ln -s output/fort.29 .
ln -s output/fort.37 .
ln -s output/fort.44 .
ln -s output/fort.80 .
ln -s output/fort.85 .
ln -s output/idl.conf .
for i in {1..11} 13 14
do
  ln -s output/intal_$i .
done
for i in {1..99}
do
  ln -s output/outtal_$i .
done
ln -s output/srf_properties .
