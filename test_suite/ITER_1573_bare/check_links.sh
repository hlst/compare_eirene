#!/usr/bin/bash

if [[ $1 -gt 1 ]]
then
    THREADS=$1
else
    THREADS=1
fi

THREADS=4

echo "Running ITER_1573 test case: Comparing $THREADS threads with serial version"

make

ln -s $EIRDIR/binRelease/eirene_iter

export OMP_NUM_THREADS=1
rm -f fort.*
./set_links.sh > /dev/null 2>&1
ln -sf input_1_strata_alone_nptsdelx$THREADS.dat input/input.dat
echo "Running EIRENE with $OMP_NUM_THREADS thread"
./eirene_iter > stdout.nptsdelx$THREADS
rm -rf output.nptsdelx$THREADS
mv output output.nptsdelx$THREADS
tail -1 stdout.nptsdelx$THREADS

export OMP_NUM_THREADS=$THREADS
rm -f fort.*
./set_links.sh > /dev/null 2>&1
ln -sf input_1_strata_alone.dat input/input.dat
echo "Running EIRENE with $OMP_NUM_THREADS threads"
./eirene_iter > stdout.$THREADS
rm -rf output.$THREADS
mv output output.$THREADS
tail -1 stdout.$THREADS

./compare_eirene.py output.nptsdelx$THREADS output.$THREADS -s

#echo "Output in stdout.nptsdelx$THREADS and dbg.$THREADS"
