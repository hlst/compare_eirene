C  CALLED FROM MAIN ROUTINE EIRENE TO PRODUCE FURTHER, 3RD PARTY
C  PRODUCED (POST PROCESSED) VOLUME TALLIES FOR PRINTOUT AND GRAPHICAL OUTPUT

C  in case of an underlying coarser grid, these additional tallies
C  are set on this coarse grid.
C
      SUBROUTINE EIRENE_OUTUSR
      USE EIRMOD_PRECISION
      USE EIRMOD_PARMMOD
      use eirmod_cgeom
      use eirmod_cgrid
      use eirmod_cpolyg
      implicit none
      integer :: iout,k,i,j

      open (newunit=iout,file='polygon.data')

      write (iout,'(4es12.4)') 0._dp,0._dp,0._dp,0._dp
      write (iout,'(12i6)') (NPOINT(1,K),NPOINT(2,K),K=1,NPPLG)

      DO I=1,NR1ST
        write (iout,'(6es12.4)') (XPOL(I,J),YPOL(I,J),J=1,NRPLG)
      end do

      close (unit=iout)
      
      RETURN
      END
