#!/bin/bash

# Run the full suite of tests

rm -rf cylinder_test
rm -rf cylinder_triang_octree_test
rm -rf cylinder_triang_no_octree_test
rm -rf 1D_cylindric_test
rm -rf 1D_elliptic_test
rm -rf 1D-H_slab_test
rm -rf 1D-H_slab_with_farming_test
rm -rf 2D-D_polygon_test
rm -rf 2D-D_polygon_NLPLG_test
rm -rf 2D-D_slab_test
rm -rf 2D-D_slab_finite_cylinder_11x11_test
rm -rf 2D-D_triang_test
rm -rf 3D-D_slab_11x11x11_test
rm -rf 3D-D_tetra_test
rm -rf ITER_1573_test

