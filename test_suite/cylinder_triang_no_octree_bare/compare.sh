#!/bin/bash

FILES=( \
#  H_slab.err
)

FILES_BIN=( \
#  fort.10 \
#  fort.11 \
#  fort.12 \
  fort.13 \
  fort.55
)

errorcode="0"

for FILE in ${FILES[*]}
do
  diff output/$FILE output_ref/$FILE > diff.out || errorcode="$?"
  if [ -s diff.out ]
  then
    echo -e "\n### $FILE ###"
    cat diff.out
  fi
done

for FILE in ${FILES_BIN[*]}
do
  diff output/$FILE output_ref/$FILE > /dev/null
  if [ $? != 0 ]
  then
    echo -e "\n### $FILE ###"
    diff <(xxd output/$FILE) <(xxd output_ref/$FILE) || errorcode="$?"
  fi
done

rm -f diff.out

exit $errorcode
