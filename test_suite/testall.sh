#!/bin/bash

if [[ -z "${EIRDIR}" ]]; then
    echo "EIRDIR undefined."
    echo "Please set variable EIRDIR to the location of your eirene install."
    exit
fi


# Delete any existing test directories
./cleantests.sh

# Run the full suite of tests
echo "Running EIRENE test cases for thread comparisons"
echo "------------------------------------------------"
./runtest.sh cylinder
echo "------------------------------------------------"
./runtest.sh cylinder_triang_octree
echo "------------------------------------------------"
./runtest.sh cylinder_triang_no_octree
echo "------------------------------------------------"
./runtest.sh 1D_cylindric
echo "------------------------------------------------"
./runtest.sh 1D_elliptic
echo "------------------------------------------------"
./runtest.sh 1D-H_slab
echo "------------------------------------------------"
./runtest.sh 1D-H_slab_with_farming
echo "------------------------------------------------"
./runtest.sh 2D-D_polygon
echo "------------------------------------------------"
./runtest.sh 2D-D_polygon_NLPLG
echo "------------------------------------------------"
./runtest.sh 2D-D_slab
echo "------------------------------------------------"
./runtest.sh 2D-D_slab_finite_cylinder_11x11
echo "------------------------------------------------"
./runtest.sh 2D-D_triang
echo "------------------------------------------------"
./runtest.sh 3D-D_slab_11x11x11
echo "------------------------------------------------"
./runtest.sh 3D-D_tetra
echo "------------------------------------------------"
./runtest.sh ITER_1573
echo "------------------------------------------------"
echo "EIRENE test cases complete"

