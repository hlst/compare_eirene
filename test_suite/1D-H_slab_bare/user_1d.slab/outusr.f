C
C
      SUBROUTINE EIRENE_OUTUSR
      USE EIRMOD_PRECISION
      USE EIRMOD_PARMMOD

      USE EIRMOD_CPLOT
      USE EIRMOD_COMUSR
      USE EIRMOD_CESTIM
      USE EIRMOD_COMXS
      USE EIRMOD_COUTAU
      USE EIRMOD_CGRID
      USE EIRMOD_CGEOM
      USE EIRMOD_CSPEZ
      USE EIRMOD_CCONA
      USE EIRMOD_CSDVI
      USE EIRMOD_COMPRT, ONLY: IUNOUT
      IMPLICIT NONE
      real(dp) :: Zconnor(nr1ST),pdena1(nr1st),
     .            sigmap(nr1st),sigmam(nr1st),sigmas(nr1st),
     .            a,b,c,ti,te,rm,vt,dx
      integer :: ir,irei,ircx,icell,ipl
c
c  output for Connor analytic solution, 1D

c  convert x into normalized 1D Connor coordinate z

      CALL EIRENE_MASBOX
     .     ('PRINTOUT FROM OUTUSR, NORMALIZED 1d COORD, CONNOR ')

C  GET ALPHA, BETA, AND C VALUE,, Te=const., Ti=const., density profile

      IREI=1
      IRCX=1
      ICELL=1
      IPL=1
      Ti=tiin(1,icell)
      Te=tein(icell)
      rm=1.00  !H mass

c  1D "thermal" ion velocity  vth=sqrt(Ti/m)  cm/s
      vt=cvel2a* sqrt(ti/rm)
c  note: eirene usually defines sqrt(2T/m) as its thermal velocity
c        this also seems to be the Connor, and Recker Wobig definition, so:
      vt=vt * sqrt(2.0)

      A=tABEI1(irei,ICELL)/DEIN(ICELL)
      b=tABCX3(irCX,ICELL,1)/DiIN(IPL,ICELL)
      c=b/(b+a)

      WRITE (IUNOUT,*) 'A,B,C,Ti,Te,vt  ',a,b,c,ti,te,vt

      
      do ir=1,nr1st-1
c   density
        pdena1(ir)=pdena(1,ir)
c   density plus 1 std. dev
        sigmap(ir)=(1.+sigma(1,ir)/100.)*pdena1(ir)
c   density minus 1 std. dev
        sigmam(ir)=(1.-sigma(1,ir)/100.)*pdena1(ir)
c   standard deviation in density units
        sigmas(ir)=sigma(1,ir)/100.*pdena1(ir)
      enddo

c   set boundary values for neutral density from linear extrapolation ??
c   to be done: from cell centered to surface centered 
      pdena1(nr1st)=pdena1(nr1st-1)
      sigmas(nr1st)=0.0

c  Connor's normalized Z coordinate,  zconnor=0 at x=rhosrf(nr1st)
      Zconnor=0.0
      do ir=nr1st-1,1,-1
        dx=rhosrf(ir+1)-rhosrf(ir)
        Zconnor(ir)=Zconnor(ir+1)+dx*(dein(ir)*(A+B)/vt)
      ENDDO

      CALL EIRENE_MASRR5('N,RHO,Z,Ne,N0,dev     ',
     .                    RHOSRF,Zconnor,dein,pdena1,sigmas,NR1ST)
      CALL EIRENE_LEER(2)

      RETURN
      END
