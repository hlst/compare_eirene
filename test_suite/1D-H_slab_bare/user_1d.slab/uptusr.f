C
      SUBROUTINE EIRENE_UPTUSR(XSTOR2,XSTORV2,WV,IFLAG)
C
C  USER SUPPLIED TRACKLENGTH ESTIMATOR, VOLUME AVERAGED

C  THIS VERSION:  
C    ADDV(IATM,ICELL)         : VOLUMETRIC CX RATE  (REACTIONS/S/CM**3), ATOMS
C    ADDV(NATMI+IATM,ICELL)   : VOLUMETRIC INCIDENT ION ENERGY CX RATE  (EV/S/CM**3), ATOMS
C    ADDV(2*NATMI+IATM,ICELL) : VOLUMETRIC NET ION ENERGY CX RATE  (EV/S/CM**3), ATOMS
C
      USE EIRMOD_PRECISION
      USE EIRMOD_PARMMOD
      USE EIRMOD_CESTIM
      USE EIRMOD_COMUSR
      USE EIRMOD_COMPRT
      USE EIRMOD_CUPD
      USE EIRMOD_COMXS
      USE EIRMOD_CSPEZ
      USE EIRMOD_CGRID
      USE EIRMOD_CLOGAU

      IMPLICIT NONE
      REAL(DP), INTENT(IN) :: XSTOR2(MSTOR1,MSTOR2,N2ND+N3RD),
     .                        XSTORV2(NSTORV,N2ND+N3RD), 
     .                        WV
      INTEGER, INTENT(IN) :: IFLAG
      INTEGER ::   ICOU,K,IRD,IACX,IRCX
      REAL(DP) ::  DIST,WTR,WTRSIG

C
C  ON INPUT:  WV=WEIGHT/VEL
C
C  ATOMS
      IF (ITYP.NE.1) RETURN

C  CHECK: STORAGE FOR AT LEAST 3 ADDITIONAL TRACKLENGTH ESTIMATED TALLIES?
      IF (NADV.LT.3*NATMI) THEN
        RETURN
      ENDIF
C
      DO 200 ICOU=1,NCOU
          DIST=CLPD(ICOU)
          WTR=WV*DIST
          IRD=NRCELL+NUPC(ICOU)*NR1P2+NBLCKA
C
          IF (LGVAC(IRD,0)) GOTO 200
C
          if (ncou.gt.1) then
            XSTOR(:,:) = XSTOR2(:,:,ICOU)
            XSTORV(:)  = XSTORV2(:,ICOU)
          endif
          
C
C
          IF (LGACX(IATM,0,0).EQ.0) GOTO 590
            DO 560 IACX=1,NACXI(IATM)
              IRCX=LGACX(IATM,IACX,0)
              IPLS=LGACX(IATM,IACX,1)
              IF (LGVAC(IRD,IPLS)) GOTO 560
c  volumetric charge exchange rate
              WTRSIG=WTR*SIGVCX(IRCX)
              ADDV(IATM,IRD)=ADDV(IATM,IRD)+WTRSIG
c  note: in case of collision estimator for energy tallies in ircx process:
c        esigcx is not set (== 0.) in fpatha.f
c  volumetric incident ion energy loss rate due to charge exchange
              ADDV(NATM+IATM,IRD)=ADDV(NATM+IATM,IRD)+
     .                            WTRSIG*ESIGCX(IRCX,1)
c  volumetric net ion energy loss rate due to charge exchange
              ADDV(2*NATM+IATM,IRD)=ADDV(2*NATM+IATM,IRD)+
     .                              WTRSIG*(ESIGCX(IRCX,1)-E0)
560         CONTINUE
590       CONTINUE
200   CONTINUE
C
C
C
      RETURN
      END
 
 
 
