CDK USER
C
C   USER SUPPLIED SUBROUTINES
C
C           *******************
C           *  Mitja Beckers  *  (system code)
C           *******************
C
      SUBROUTINE EIRENE_PROUSR (PRO,INDX,P0,P1,P2,P3,P4,P5,PROVAC,N)

C   radial grid size:  N, i.e. N-1 cells.
C   SET RADIAL PROFILE PRO, PRO(I=1,N-1) WITH PRO(N-1)=PROVAC
C   RADIAL GRID IS SET IN INPUT BLOCK 2.  RGA,RAA,RRA.
C   THESE DEFINE GRID RESOLUTION.
C   THE SYSTEM CODE PARAMETERS: MINOR RADIUS, RHO_PEDESTAL, DELTA_SOL
c   then use that grid (rhozne) "as is", i.e. the rho_pedestal and delta_sol need not be
c   related to raa,rga and rra
C   P0,P5: NOT IN USE
C   READ PP1,...PP12,  PROFILE PARAMETERS

C  INDX = 1+1*NPLS:  DIIN
C
C     PP1 : CENTRAL VALUE             
C     PP2 : RADIAL POSITION OF TOP OF PEDESTAL, NORMALIZED TO RHO_SEP=1
C     PP3 : CORE PROFILE SHAPE PARAMETER
C     PP4 : RADIAL POSITION OF SEPARATRIX (CM), CORRESP. TO RHO=1                     
C     PP5 : VALUE AT TOP OF PEDESTAL
C     PP6 : VALUE AT SEPARATIX (=BOTTOM OF PEDESTAL), CORRESP. TO RHO=1                    
C     PP7 : 
C     PP8 : 
C     PP9 :                      
C     PP10 : DECAY LENGTH IN SOL (CM)
C     PP11 : MINIMUM IN FAR SOL
C     PP12 : MULTIPLICATIVE FACTOR FOR 
      USE EIRMOD_PRECISION
      USE EIRMOD_PARMMOD
      USE EIRMOD_COMUSR
      USE EIRMOD_COMPRT
      USE EIRMOD_CGRID
      USE EIRMOD_CGEOM
      IMPLICIT NONE
      REAL(DP), INTENT(IN) :: P0, P1, P2, P3, P4, P5, PROVAC
      REAL(DP), INTENT(OUT) :: PRO(*)
      INTEGER, INTENT(IN) :: INDX, N

      REAL(DP) :: PP1, PP2, PP3, PP4, PP5, PP6,
     .            PP7, PP8, PP9, PP10, PP11, PP12,
     .            RHO, SLOPE, RHOTOP,PRO0,DELR
      CHARACTER(80) :: ZEILE
      INTEGER IFIRST,I
      data ifirst /0/
C  READ ZEILE:  *** prousr
      IF (IFIRST.EQ.0) THEN 
        READ (IUNIN,'(A80)') ZEILE
        CALL EIRENE_LEER(1)

        WRITE (IUNOUT,*) ZEILE
        IFIRST=1
      ENDIF

      READ (IUNIN,'(A80)') ZEILE  !  DENSITY, TEMPERATURE, ETC... PROFILE 
      CALL EIRENE_LEER(1)
      WRITE (IUNOUT,*) ZEILE

      read (IUNIN,*) PP1,PP2,PP3,PP4,PP5,PP6
      read (IUNIN,*) PP7,PP8,PP9,PP10,PP11,PP12



      WRITE (IUNOUT,*) 'p1: central value (on magn. axis)       ',PP1
      WRITE (IUNOUT,*) 'p2: rho_pedestal, between 0 and 1       ',PP2
      WRITE (IUNOUT,*) 'p3: core profile shape parameter        ',PP3
      WRITE (IUNOUT,*) 'p4: minor radius (cm), LCFS (rho=1)     ',PP4
      WRITE (IUNOUT,*) 'p5: value at pedestal top               ',PP5
      WRITE (IUNOUT,*) 'p6: value at pedestal bottom (at rho=1) ',PP6

      WRITE (IUNOUT,*) 'p7,p8,p9: currently not in use         '
      WRITE (IUNOUT,*) 'p10: exp. decay length at rho .ge. 1    ',PP10
      WRITE (IUNOUT,*) 'p11: ceiling (minimum) or profile in SOL',PP11
      WRITE (IUNOUT,*) 'p12: multiplicative factor for profile  ',PP12

      RHOTOP=PP5

cdr   WRITE (IUNOUT,*) 'RHOTOP,   ',RHOTOP

      DO I=1,N-1
cvac  in case one vacuum zone is set (rra .gt.raa), this must be I=1,N-2
        RHO=RHOZNE(I)/PP4  !   turn radial grid point into relative "rho" grid

        IF (RHO.LE.PP2) THEN         
C  CORE PROFILE
          PRO(I)= PP5+(PP1-PP5)*(1.0-(RHO/PP2)**2)**PP3
        ELSEIF (RHO.GT.PP2.AND.RHO.LE.1.0) THEN
C  PEDESTAL PROFILE, LINEAR DECAY FROM TOP OF PEDESTAL TO SEPARATRIX     
          SLOPE= (RHOTOP-PP6)/(PP2-1.0)  ! NEGATIVE SLOPE, separatrix corresponds to rho=1.
          PRO(I)= RHOTOP+SLOPE*(RHO-PP2)           
        ELSEIF (RHO.GT.1.0) THEN  
C  SOL PROFILE
          PRO0= PP6 ! VALUE AT RHO=1 (SEPARATRIX), FROM HERE: EXPONENTIAL DECAY, UNTIL CEILING
          DELR=RHOZNE(I)-PP4  ! DISTRANCE INTO SOL, CM
          PRO(I)=MAX(PP11,PRO0*EXP(-DELR/PP10))
        ENDIF
      ENDDO
C  MULTIPLICATIVE FACTOR
      DO I=1,N-1
cvac  in case one vacuum zone is set (rra .gt.raa), this must be I=1,N-2
        PRO(I)=PP12*PRO(I)
      ENDDO
cvac  PRO(N-1)=PROVAC
 
      RETURN
      END
