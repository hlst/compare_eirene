C  user supplied collision estimator.
C  called from folion, folneut, at a collision event in cell NCELL 
C
      SUBROUTINE EIRENE_UPCUSR(WS,IND)
C
C  USER SUPPLIED COLLISION ESTIMATOR, VOLUME AVERAGED
C   
C  ON INPUT:
C     WS=WEIGHT/SIGTOT=WEIGHT/(VEL*ZMFPI)=WEIGHT/(SIGMA-V*DENS)
C     IND =1  PRE-COLLISION ESTIMATOR,  called prior to call to subr. COLLIDE
C     IND =2  POST-COLLISION ESTIMATOR, called after call to subr. COLLIDE
C
C  via modules:  NCELL, IATM
C
C
      USE EIRMOD_PRECISION
      USE EIRMOD_PARMMOD
      USE EIRMOD_CESTIM
      USE EIRMOD_COMUSR
      USE EIRMOD_COMPRT
      USE EIRMOD_COMXS
      IMPLICIT NONE
      REAL(DP), INTENT(IN) :: WS
      INTEGER, INTENT(IN) :: IND
      INTEGER :: IRCX,IACX,ityp_save=0
      REAL(DP) :: WSSIG
C

C
C  FOR PARTICLE DENSITY IN CELL NO. NCELL
C     COLV(1,NCELL)=COLV(1,NCELL)+WS

C   THIS VERSION: POST COLLISION BULK ION CX ENERGY LOSS RATE
C   E0 IS THE ENERGY SAMPLED FOR THE POST COLLISION PARTICLE
c   ind=1: for particle density
c   ind=2: for post cx neutral energy rate = pre-cx ion energy rate


      if (ind.eq.1) then
        ityp_save=ityp
      endif


      IF (ITYP.NE.1) RETURN

C  CHECK: STORAGE FOR AT LEAST 2 COLLISION ESTIMATED TALLIES PER ATOMIC SPECIES
      IF (NCLV.LT.2*NATMI) THEN
        RETURN
      ENDIF
C
      IF (LGACX(IATM,0,0).EQ.0) GOTO 590
        DO 560 IACX=1,NACXI(IATM)
          IRCX=LGACX(IATM,IACX,0)
          IPLS=LGACX(IATM,IACX,1)
          IF (LGVAC(NCELL,IPLS)) GOTO 560
          WSSIG=WS*SIGVCX(IRCX)
C  TALLY 1: PARTICLE DENSITY (FOR TESTING/VERIFICAITON)
          if (ind.eq.1) COLV(IATM,NCELL)=COLV(IATM,NCELL)+WS
C  TALLY 2:  CX-ION INCIDENT ENERGY COLLISION RATE, POST COLLISION atoms, only from pre collision atoms       
          if (ind.eq.2.and.ityp_save.eq.1) then 
            COLV(NATM+IATM,NCELL)=COLV(NATM+IATM,NCELL)+
     .                            WSSIG*E0
            ityp_save=0
          endif
560     CONTINUE
590   CONTINUE
C
      RETURN
      END
