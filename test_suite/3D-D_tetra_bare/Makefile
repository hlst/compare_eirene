FC = mpif90
FFLAGS = -O3 -fopenmp -cpp 
BIN = ./eirene
SRCDIR = user_3d.tetra
CONFIG ?= Release
EIRDIR ?= ../..
EIRMAINSRC ?= $(EIRDIR)/src/main-routines
EIRMOD ?= $(EIRDIR)/build$(CONFIG)
EIRLIB ?= $(EIRDIR)/lib$(CONFIG)

VPATH = $(SRCDIR) $(EIRMAINSRC)

SRC := $(wildcard $(SRCDIR)/*.f) $(EIRMAINSRC)/eirene_main.f
OBJ := $(notdir $(SRC:.f=.o))

all: $(BIN)

bin: $(BIN)

$(BIN): $(OBJ)
	$(FC) $(FFLAGS) -o $(BIN) $(OBJ) -L $(EIRLIB) -l EIRENE

%.o: %.f
	$(FC) $(FFLAGS) -c -I $(EIRMOD) $<

output: $(BIN)
	./set_links.sh ;\
	$(BIN) 1>3d_tetra.reference.out 2>3d_tetra.reference.err || errorcode="$$?" ;\
	./rm_links.sh ;\
	exit $$errorcode

test: output
	./compare.sh

coverage.info: output
	lcov --capture --directory $(EIRMOD) --output-file coverage.info
	lcov --zerocounters --directory $(EIRMOD)

clean: cleanoutput cleancoverage
	$(RM) $(BIN) $(OBJ)

cleanoutput:
	$(RM) -r output

cleancoverage:
	$(RM) coverage.info

.PHONY: all bin test clean cleanoutput cleancoverage
