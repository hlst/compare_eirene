CDK USER
C
C   USER SUPPLIED SUBROUTINES
C

c  read in plasma profiles for tetrahedron mesh
c  read from fort.31
C
      SUBROUTINE EIRENE_PROUSR (PRO,INDX,P0,P1,P2,P3,P4,P5,PROVAC,N)
C 
C  EXAMPLE:
C     P0 : CENTRAL VALUE
C     P1 : STARTING RADIUS FOR POLYNOMIAL
C     P2 : SWITCH FOR PHASE 1: OH-PHASE
C                           2: NI-PHASE
C     P3 : FACTOR FOR TI: TI=K*TE
C
      USE EIRMOD_PRECISION
      USE EIRMOD_PARMMOD
      USE EIRMOD_COMUSR
      USE EIRMOD_COMPRT
      USE EIRMOD_CGRID
      USE EIRMOD_CGEOM
      USE EIRMOD_CTETRA
      USE EIRMOD_CINIT
      IMPLICIT NONE
      REAL(DP), INTENT(IN) :: P0, P1, P2, P3, P4, P5, PROVAC
      REAL(DP), INTENT(OUT) :: PRO(*)
      INTEGER, INTENT(IN) :: INDX, N
 
      integer, save :: ifirst=0
      integer :: i, j
      real(dp), allocatable, save :: te(:),ti(:),di(:),
     .                               vx(:),vy(:),vz(:), 
     .                               bx(:),by(:),bz(:),bf(:)
      
      if (ifirst == 0) then

C......................................................
         CALL EIRENE_LEER(2)
         write (iunout,*) ' PRINTOUT FROM PROUSR, READ FORT.31  '
         CALL EIRENE_LEER(1)
C........................................................

         ifirst = 1
         open (unit=31,file=trim(CASENAME)//'.plasma',
     .         ACCESS='SEQUENTIAL',FORM='FORMATTED')

         write (iunout,*) 'open file ',trim(CASENAME)//'.plasma'
         allocate(te(nrad),source=0._dp)
         allocate(ti(nrad),source=0._dp)
         allocate(di(nrad),source=0._dp)
         allocate(vx(nrad),source=0._dp)
         allocate(vy(nrad),source=0._dp)
         allocate(vz(nrad),source=0._dp)
         allocate(bx(nrad),source=0._dp)
         allocate(by(nrad),source=0._dp)
         allocate(bz(nrad),source=1._dp)
         allocate(bf(nrad),source=1._dp)

         do i=1,ntet-1
           read (31,*) j,te(i),ti(i),di(i),
     .                   vx(i),vy(i),vz(i),
     .                   bx(i),by(i),bz(i),bf(i)
         end do
      end if

      pro(1:n) = 0

      if (indx == 0) then
        pro(1:n) = te(1:n)
      elseif (indx == 1) then
        pro(1:n) = ti(1:n)
      elseif (indx == 1+1*npls) then
        pro(1:n) = di(1:n)
      elseif (indx == 1+2*npls) then
        pro(1:n) = vx(1:n)
      elseif (indx == 1+3*npls) then
        pro(1:n) = vy(1:n)
      elseif (indx == 1+4*npls) then
        pro(1:n) = vz(1:n)
      elseif (indx == 1+1*npls+nplsti+3*nplsv) then
        write (6,*) 'bx(1) ',bx(1)
        pro(1:n) = bx(1:n)
      elseif (indx == 2+1*npls+nplsti+3*nplsv) then
        write (6,*) 'by(1) ',by(1)
        pro(1:n) = by(1:n)
      elseif (indx == 3+1*npls+nplsti+3*nplsv) then
        write (6,*) 'bz(1) ',bz(1)
        pro(1:n) = bz(1:n)
      elseif (indx == 4+1*npls+nplsti+3*nplsv) then
        write (6,*) 'bf(1) ',bf(1)
        pro(1:n) = bf(1:n)
      end if

      END
