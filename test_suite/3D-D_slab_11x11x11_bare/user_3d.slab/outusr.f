C  CALLED FROM MAIN ROUTINE EIRENE TO PRODUCE FURTHER, 3RD PARTY
C  PRODUCED (POST PROCESSED) VOLUME TALLIES FOR PRINTOUT AND GRAPHICAL OUTPUT

C  in case of an underlying coarser grid, these additional tallies
C  are set on this coarse grid.
C
      SUBROUTINE EIRENE_OUTUSR
      USE EIRMOD_PRECISION
      USE EIRMOD_PARMMOD

      USE EIRMOD_CPLOT
      USE EIRMOD_COMUSR
      USE EIRMOD_CESTIM
      USE EIRMOD_COUTAU
      USE EIRMOD_CGRID
      USE EIRMOD_CGEOM
      USE EIRMOD_CSPEZ
      USE EIRMOD_CTETRA
      USE EIRMOD_CADGEO
      USE EIRMOD_module_avltree
      USE EIRMOD_PLTEIR, ONLY: EIRENE_PLTEIR
      IMPLICIT NONE
      real(dp) :: dummy(nrtal)
      integer :: i, ir, ip, it, ir1, ip1, it1, ncll, ntact
      integer :: ic1, ic2, ic3, ic4, ic5, ic6, ic7, ic8,
     .           i1, i2, i3, i4 
      type(TAVLTree), pointer, save :: baum
C
      INTERFACE 
        SUBROUTINE EIRENE_INSERT_POINT(baum,X,Y,Z,DIST,IC1)
          USE EIRMOD_PRECISION
          USE EIRMOD_module_avltree
          type(TAVLTree), pointer :: baum
          REAL(DP), INTENT(IN)      :: X, Y, Z, DIST
          INTEGER, INTENT(INOUT)  :: IC1
        END SUBROUTINE EIRENE_INSERT_POINT
      END INTERFACE

      ntetra = nsbox*6
      ncoord = nsbox

      call eirene_dealloc_ctetra
      call eirene_alloc_ctetra

      NTET = 0
      NCOOR = 0
      NTBAR = 0
      NTSEITE = 0
      ALLOCATE (COORTET(NCOORD))
      DO I=1,NCOORD
        NULLIFY(COORTET(I)%PTET)
      END DO
      baum => Eirene_NewTree()

      deallocate(ncltal)
      allocate(ncltal(ntetra))
      
      it = 0
      ncll = 0

      do it = 1, nt3rd-1
        IT1 = IT+1

        do ip = 1, np2nd-1
          IP1=IP+1
          do ir = 1, nr1st-1
            IR1=IR+1
            if (.false.) then
!  P1   (ir,ip,it)
            CALL EIRENE_INSERT_POINT(baum,rsurf(IR),
     .             psurf(IP),Zsurf(IT),1._DP,IC1)
!  P2   (ir+1,ip,it)
            CALL EIRENE_INSERT_POINT(baum,rsurf(IR1),
     .             psurf(IP),Zsurf(IT),1._DP,IC2)
!  P3   (ir+1,ip,it+1)
            CALL EIRENE_INSERT_POINT(baum,rsurf(IR1),
     .             psurf(IP),Zsurf(IT1),1._DP,IC3)
!  P4   (ir,ip,it+1)
            CALL EIRENE_INSERT_POINT(baum,rsurf(IR),
     .             psurf(IP),Zsurf(IT1),1._DP,IC4)
!  P5   (ir,ip+1,it)
            CALL EIRENE_INSERT_POINT(baum,rsurf(IR),
     .             psurf(IP1),Zsurf(IT),1._DP,IC5)
!  P6   (ir+1,ip+1,it)
            CALL EIRENE_INSERT_POINT(baum,rsurf(IR1),
     .             psurf(IP1),Zsurf(IT),1._DP,IC6)
!  P7   (ir+1,ip+1,it+1)
            CALL EIRENE_INSERT_POINT(baum,rsurf(IR1),
     .             psurf(IP1),Zsurf(IT1),1._DP,IC7)
!  P8   (ir,ip+1,it+1)
            CALL EIRENE_INSERT_POINT(baum,rsurf(IR),
     .             psurf(IP1),Zsurf(IT1),1._DP,IC8)

            else
              CALL EIRENE_INSERT_POINT(baum,rsurf(IR),
     .             psurf(IP),Zsurf(IT),1._DP,IC1)
              CALL EIRENE_INSERT_POINT(baum,rsurf(IR1),
     .             psurf(IP),Zsurf(IT),1._DP,IC2)
              CALL EIRENE_INSERT_POINT(baum,rsurf(IR1),
     .             psurf(IP1),Zsurf(IT),1._DP,IC3)
              CALL EIRENE_INSERT_POINT(baum,rsurf(IR),
     .             psurf(IP1),Zsurf(IT),1._DP,IC4)
              CALL EIRENE_INSERT_POINT(baum,rsurf(IR),
     .             psurf(IP),Zsurf(IT1),1._DP,IC5)
              CALL EIRENE_INSERT_POINT(baum,rsurf(IR1),
     .             psurf(IP),Zsurf(IT1),1._DP,IC6)
              CALL EIRENE_INSERT_POINT(baum,rsurf(IR1),
     .             psurf(IP1),Zsurf(IT1),1._DP,IC7)
              CALL EIRENE_INSERT_POINT(baum,rsurf(IR),
     .             psurf(IP1),Zsurf(IT1),1._DP,IC8)
            end if
            
            ncoor = max(ncoor,ic1,ic2,ic3,ic4,ic5,ic6,ic7,ic8)
            ncll = ir+((ip-1)+(it-1)*np2nd)*nr1st

            CALL EIRENE_MAKE_TETRA(IC1,IC2,IC3,IC4,IC5,IC6,IC7,IC8,
     .             ir,ip,it)

            NTACT=NTET-6
              
            ncltAL(ntact+1:ntet) = ncll

          end do

        end do
      end do

      call eirene_suche_nachbarn
      
!      WRITE(17+ifoff,'(1X,A5,8X,A4,11X,A1,11X,A1,11X,A1,11X,A1)') 
!     .        '-1111','NPCO','1','3','1','1'
      write (17+ifoff,'(I6)') ncoor
      do i=1,ncoor
        WRITE(17+ifoff,'(I6,1P,3E12.4)')
     .          I,XTETRA(I),YTETRA(I),ZTETRA(I)
      enddo
!      WRITE(17+ifoff,'(1X,A5,8X,A3,12X,A1,11X,A1,11X,A1,11X,A1)') 
!     .           '-9999','FIN','0','0','0','0'

!      WRITE(18+ifoff,'(A3,I3,A60,I6)') 'PSS',1,'BEISPIELDATEN',-3334
!      WRITE(18+ifoff,'(A8,I6,I9,I6)') 'TET4    ',1,NTET,4
      write (18+ifoff,'(I6)') ntet
      write (19+ifoff,'(I6)') ntet
      do i=1,ntet
        WRITE(18+ifoff,'(1X,A1,4I10)') '0',NTECK(1,I),NTECK(2,I),
     .                                     NTECK(3,I),NTECK(4,I)
        i1 = -inmtit(1,i)
        i2 = -inmtit(2,i)
        i3 = -inmtit(3,i)
        i4 = -inmtit(4,i)
!        if (i1 > nlimi) i1 = -(i1-nlim)
!        if (i2 > nlimi) i2 = -(i2-nlim)
!        if (i3 > nlimi) i3 = -(i3-nlim)
!        if (i4 > nlimi) i4 = -(i4-nlim)
        WRITE(19+ifoff,'(1X,i6,4(3i6,3x))') i,
     .       NTBAR(1,I),NTSEITE(1,I),I1,
     .       NTBAR(2,I),NTSEITE(2,I),I2,
     .       NTBAR(3,I),NTSEITE(3,I),I3,
     .       NTBAR(4,I),NTSEITE(4,I),I4
        ncll = ncltal(i)
        write (31,'(i6,10es22.15)')
     .      i,tein(ncll),tiin(1,ncll),diin(1,ncll),
     .        vxin(1,ncll), vyin(1,ncll),vzin(1,ncll),
     .        bxin(ncll), byin(ncll),bzin(ncll),bfin(ncll)
      enddo
        
      call eirene_DestroyTree(baum)
      
      RETURN
      END
