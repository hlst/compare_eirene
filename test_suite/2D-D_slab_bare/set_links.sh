#!/bin/bash

if [ -z ${EIRDBASE+x} ]
then
  if [ -z ${EIRDIR+x} ]
  then
    EIRDIR=../..
  fi
  EIRDBASE=$EIRDIR/Database
fi

ln -s input/slab_2d.reference.dat fort.1
ln -s input/block45-2d.reference block45

ln -s $EIRDBASE/AMdata/amjuel.tex AMJUEL
ln -s $EIRDBASE/AMdata/hydhel.tex HYDHEL
ln -s $EIRDBASE/Surfacedata/SPUTER .
ln -s $EIRDBASE/Surfacedata/TRIM/D_on_C .
ln -s $EIRDBASE/Surfacedata/TRIM/D_on_W .
ln -s $EIRDBASE/Surfacedata/TRIM/D_on_W_5.53 .

mkdir -p output
ln -s output/eirene-2d.reference.out .
ln -s output/eirene-2d.reference.err .
ln -s output/fort.10 .
ln -s output/fort.11 .
ln -s output/fort.12 .
ln -s output/fort.55 .
ln -s output/fort.100 .
ln -s output/fort.101 .
ln -s output/fort.102 .
ln -s output/fort.103 .
