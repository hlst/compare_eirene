CDK USER
C
C   USER SUPPLIED SUBROUTINES
C
C
      SUBROUTINE EIRENE_PROUSR (PRO,INDX,P0,P1,P2,P3,P4,P5,PROVAC,N)

c  user defined backgound (plasma) profiles.
c  called from routine plasma.f, indpro=5 option
c  input:  indx  : indicates which input tally (e.g. Te, ni, vx, etc...)
c          provac: eirene default vacuum value (e.g. dvac, tvac, etc...)
c          N     : N=NSURF, standard grid without additional cell region
C 
C  EXAMPLE:
c  mache exp. dichteprofil in y richtung, und dichte in x richtung konstant.
c     tbd. : genauer:  indx anschliesen...

C     P0 : CENTRAL VALUE  (at psurf(1))
C     P1 : outer   value  (at psurf(np2nd)) , profile: exponential decay
c    indx not used, da eh nur ein aufruf: indpro=5 genau nur fuer dichteprofil
c                   im anwendungsfall hier.

C
      USE EIRMOD_PRECISION
      USE EIRMOD_PARMMOD
      USE EIRMOD_COMUSR
      USE EIRMOD_CGRID
      USE EIRMOD_CGEOM
      USE EIRMOD_CPOLYG
      USE EIRMOD_CTRIG
      USE EIRMOD_COMPRT, ONLY: IUNOUT
      IMPLICIT NONE
      REAL(DP), INTENT(IN) :: P0, P1, P2, P3, P4, P5, PROVAC
      REAL(DP), INTENT(OUT) :: PRO(*)
      INTEGER, INTENT(IN) :: INDX, N
      integer  :: i,j, ic
      real(dp) :: E, densj, rin0, a1, sep, dist, prosep, t, ymin, ymax
      

      PRO(1:N)=0.0

      if (indx <= 1) then
        rin0 = p1
        a1 = p2
        E = p4
        sep = p5
        dist = sep - rin0
        prosep = p0*exp(-dist/a1)

        if (levgeo == 3) then
          do j=1,np2nd-1
            do i=1,nr1st-1
              ic = i+nr1st*(j-1)
              if (xcom(ic) <= sep) then
                dist = xcom(ic) - rin0
                t= p0*exp(-dist/a1)
                pro(ic)= t
              else
                dist = xcom(ic) - sep
                t= prosep*exp(-dist/E)
                pro(ic)= t
              end if
            enddo
          enddo
        elseif (levgeo == 4) then
          do i=1,ntri
            ic = ncltal(i)
            if (ic == 0) cycle
            if (xcom(ic) <= sep) then
              dist = xcom(ic) - rin0
              t= p0*exp(-dist/a1)
              pro(ic)= t
            else
              dist = xcom(ic) - sep
              t= prosep*exp(-dist/E)
              pro(ic)= t
            end if
          end do
        else
          write (iunout,*) ' PROUSR called for Te or Ti',
     .                     ' with LEVGEO.NE.3'
          WRITE (iunout,*) 'NO PLASMA PARAMETERS RETURNED'
        end if
         
      else

        if (levgeo == 1) then
c   find decay length E in y direction
          E=-(psurf(np2nd)-psurf(1))/(log(p1)-log(p0))
           
c set density constant in x direction and exp profile in y direction (2nd grid)
          do i=1,nr1st-1
            do j=1,np2nd-1
              densj= p0*exp(-(phzone(j)-psurf(1))/e)
              pro(i+nr1st*(j-1))= densj
            enddo
          enddo   

        else if (levgeo == 3) then
c   find decay length E in y direction
          E=-(ypol(1,np2nd)-ypol(1,1))/(log(p1)-log(p0))
           
c set density constant in x direction and exp profile in y direction (2nd grid)
          do i=1,nr1st-1
            do j=1,np2nd-1
              ic = i+nr1st*(j-1)
              densj= p0*exp(-(ycom(ic)-ypol(1,1))/e)
              pro(ic)= densj
            enddo
          enddo   
        else if (levgeo == 4) then
c   find decay length E in y direction
          ymax = maxval(ytrian(1:nknots))
          ymin = minval(ytrian(1:nknots))
          E=-(ymax-ymin)/(log(p1)-log(p0))
           
c set density constant in x direction and exp profile in y direction (2nd grid)
          do i=1,ntri
            ic = ncltal(i)
            if (ic == 0) cycle
            densj= p0*exp(-(ycom(ic)-ymin)/e)
            pro(ic)= densj
          enddo   
        end if
      end if

      RETURN
      END
