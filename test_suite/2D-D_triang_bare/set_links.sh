#!/bin/bash

if [ -z ${EIRDBASE+x} ]
then
  if [ -z ${EIRDIR+x} ]
  then
    EIRDIR=../..
  fi
  EIRDBASE=$EIRDIR/Database
fi

ln -s input/triangles_2d.reference.data fort.1
ln -s input/2D-D_triangles.npco_char 2D-D_triangles.npco_char
ln -s input/2D-D_triangles.elemente 2D-D_triangles.elemente
ln -s input/2D-D_triangles.neighbors 2D-D_triangles.neighbors
ln -s input/block45-2d.reference block45

ln -s $EIRDBASE/AMdata/amjuel.tex AMJUEL
ln -s $EIRDBASE/AMdata/hydhel.tex HYDHEL
ln -s $EIRDBASE/Surfacedata/SPUTER .
ln -s $EIRDBASE/Surfacedata/TRIM/D_on_W .
ln -s $EIRDBASE/Surfacedata/TRIM/D_on_C .
ln -s $EIRDBASE/Surfacedata/TRIM/D_on_W_5.53 .

mkdir -p output
ln -s output/D_triang.out .
ln -s output/D_triang.err .
ln -s output/fort.10 .
ln -s output/fort.11 .
ln -s output/fort.12 .
ln -s output/fort.29 .
ln -s output/fort.55 .
ln -s output/fort.100 .
ln -s output/fort.101 .
ln -s output/fort.102 .
