#!/bin/bash

if [ -z ${EIRDBASE+x} ]
then
  if [ -z ${EIRDIR+x} ]
  then
    EIRDIR=../..
  fi
  EIRDBASE=$EIRDIR/Database
fi

ln -s input/input.eir fort.1
ln -s input/block45-H .

ln -s $EIRDBASE/AMdata/amjuel.tex AMJUEL
ln -s $EIRDBASE/AMdata/hydhel.tex HYDHEL
ln -s $EIRDBASE/Surfacedata/SPUTER .
ln -s $EIRDBASE/Surfacedata/TRIM .

mkdir -p output
ln -s output/stdout .
ln -s output/stderr .
ln -s output/fort.10 .
ln -s output/fort.11 .
ln -s output/fort.12 .
ln -s output/fort.13 .
ln -s output/fort.55 .
