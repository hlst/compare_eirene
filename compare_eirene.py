#!/bin/python
#################################################################################################
# Numerically compares two sets of eirene output files and flags any differences. Differences
# less than than machine precision (1e-15) issue a single warning, differences larger than
# this are output to stdout
# At 1e-16 errors in converting strings to doubles in python create false machine precision errors
# At 1e-15 some real machine precision errors are missed
#################################################################################################

import argparse
import numpy as np
import math
import sys
import os

################################################################################################
# Class to read and compare 2 files
#
################################################################################################

class comparison:
    def __init__( self , args ):
        """Reads 2 output files and compares the contents"""
        self.err            = 0
        self.set_tolerance( args )
        self.dir1           = args.dir1
        self.dir2           = args.dir2
        self.mp_diff        = False
        self.mp_count       = 0
        self.stat_diff      = False
        self.stat_count     = 0 
        self.warn_diff      = False
        self.warn_count     = 0
        self.err_diff       = False
        self.err_count      = 0
        self.quiet          = args.quiet
        self.verbose        = args.verbose
        self.warnings       = args.warnings
        self.errall         = args.errall
        self.ignore         = args.ignore_small_values
        self.ignorefac      = args.ignore_factor
        self.nobinary         = not args.nobinary
        self.count1         = 0
        self.count2         = 0
        self.iter_files     = ['intal_1','intal_2','intal_3','intal_4','intal_5','intal_6','intal_7','intal_8','intal_9','intal_10','intal_11','intal_12','intal_13','intal_14','outtal_1','outtal_2','outtal_3','outtal_5','outtal_6','outtal_7','outtal_9','outtal_10','outtal_11','outtal_12','outtal_14','outtal_15','outtal_16','outtal_17','outtal_18','outtal_20','outtal_21','outtal_22','outtal_23','outtal_24','outtal_26','outtal_33','outtal_34','outtal_35','outtal_36','outtal_38','outtal_39','outtal_40','outtal_41','outtal_42','outtal_44','outtal_45','outtal_46','outtal_47','outtal_48','outtal_50','outtal_57','outtal_60','outtal_61','outtal_75','outtal_76','outtal_77','outtal_79','outtal_80','outtal_81','outtal_82','outtal_84','outtal_85','outtal_86','outtal_87','outtal_89','outtal_90','outtal_91','outtal_93','outtal_94','outtal_95','outtal_97','outtal_98','outtal_99']
#        self.iter_files     = ['outtal_1','outtal_2','outtal_3','outtal_5','outtal_6','outtal_7','outtal_9','outtal_10','outtal_11','outtal_12','outtal_14','outtal_15','outtal_16','outtal_17','outtal_18','outtal_20','outtal_21','outtal_22','outtal_23','outtal_24','outtal_26','outtal_33','outtal_34','outtal_35','outtal_36','outtal_38','outtal_39','outtal_40','outtal_41','outtal_42','outtal_44','outtal_45','outtal_46','outtal_47','outtal_48','outtal_50','outtal_57','outtal_60','outtal_61','outtal_75','outtal_76','outtal_77','outtal_79','outtal_80','outtal_81','outtal_82','outtal_84','outtal_85','outtal_86','outtal_87','outtal_89','outtal_90','outtal_91','outtal_93','outtal_94','outtal_95','outtal_97','outtal_98','outtal_99']
        self.slab_files     = ['fort.100','fort.101','fort.102','fort.103']
        self.binary_files   = ['fort.10','fort.11','fort.12','fort.13']
        self.iter           = False

    def set_tolerance( self , args ):
        # These tolerances are rather arbitrary
        if args.single_precision:        
            self.mp_tolerance   = 1e-7
            self.stat_tolerance = 1e-6
            self.warn_tolerance = 1e-5
            self.err_tolerance  = 1e-3
        else:        
            self.mp_tolerance   = 1e-15
            self.stat_tolerance = 1e-14
            self.warn_tolerance = 1e-13
            self.err_tolerance  = 1e-3

        
    def reset( self ):
        self.mp_count   = 0 
        self.stat_count = 0 
        self.warn_count = 0 
        self.err_count  = 0 
        self.count1     = 0
        self.count2     = 0

        
    def finish( self ):
        if self.err_diff:
            print( "Large errors greater than", self.err_tolerance , "were found between output directories" )
        elif self.warn_diff:
            print( "Differences between", self.warn_tolerance , "and" , self.err_tolerance , "were found between output directories" )
        elif self.stat_diff:
            print( "Small differences between", self.stat_tolerance , "and" , self.warn_tolerance , "were found between output directories" )
        elif self.mp_diff:
            print( "Machine precision differences between" , self.mp_tolerance , "and" , self.stat_tolerance, "were found between output directories" )
        elif not self.quiet:
            print( "No differences above" , self.mp_tolerance , "were found")
           
    def report_diff( self , file ):
        if self.mp_count:
            print("File" , file , "contains" , self.mp_count , "machine precision differences from " , self.count1 , "data points" )
        if self.stat_count:
            print("File" , file , "contains" , self.stat_count , "small differences from " , self.count1 , "data points" )
            self.err = 1
        if self.warn_count:
            print("File" , file , "contains" , self.warn_count , "significant differences from " , self.count1 , "data points" )
            self.err = 2
        if self.err_count:
            print("File" , file , "contains" , self.err_count , "large errors from " , self.count1 , "data points"  )
            self.err = 3
        if self.ignore:
            print( "Average value = " , self.mean , "Values less than" , self.mean * self.ignorefac , "were ignored" )


            
    def compare_file( self , file ):
        # Open the two files and store in numpy arrays
        data1 = self.read_data( self.dir1 , file )
        data2 = self.read_data( self.dir2 , file )
        self.count1 = len(data1)
        self.count2 = len(data2)
        if self.count1 == 0:
            if self.verbose:
                print( "File " , self.dir1+file , "contains no data" ) 
            return False
        if self.verbose:
            print( "Comparing " , file )
        if self.count1 != self.count2:
            if not self.quiet:
                print( "Error: files ", self.dir1+"/"+file , "and" , self.dir2+"/"+file , "are different sizes" )
            self.err = True
            return
        if self.ignore:
            self.mean = np.sqrt(np.mean(np.square(~np.isnan(data1[np.nonzero(data1)]))))
#            self.mean = np.mean(abs(~np.isnan(data1[np.nonzero(data1)])))
#            self.mean = np.mean(~np.isnan(data1[np.nonzero(data1)]))
        if self.verbose:
            print( "Comparing" , self.count1 , "to" , self.count2 , "data points" )
        for i, val in enumerate( data1 ):
            if math.isnan(val) and math.isnan(data2[i]):
                continue
            if self.ignore:
                if val < self.mean * self.ignorefac:
                    continue
            if val != data2[i] :
                if val == 0:
                    diff = abs(data2[i])
                else:
                    if data2[i] == 0:
                        diff = val
                    else:
                        diff = abs((val-data2[i])/val)
                if diff > self.err_tolerance:
                    self.err_diff  = True
                    self.err_count += 1
                    if self.warnings:
                        print( "Error in value",i, val, data2[i] )
                elif diff > self.warn_tolerance:
                    self.warn_diff  = True
                    self.warn_count += 1
                    if self.warnings:
                        print( "Warning difference in value",i, val, data2[i] )
                elif diff > self.stat_tolerance:
                    self.stat_diff = True
                    self.stat_count += 1
                    if self.warnings:
                        print( "Value",i,"differs by more than machine precision", val, data2[i] )
                elif diff > self.mp_tolerance:
                    self.mp_diff   = True
                    self.mp_count += 1
                    if self.errall:
                        print( "Value",i,"differs at machine precision", val, data2[i] )
        if self.mp_count > 0 or self.stat_count > 0 or self.warn_count > 0 or self.err_count > 0:
            return True
        else:
            return False

# Routines for reading different data formats
    def read_iter( self , dir , file ):
        ldata = []
        dataline = False
        with open( dir+'/'+file , 'r' ) as inp:
            for line in inp:
                if dataline:
                    if "==" in line:
                        dataline = False
                    else:
                        for val in line.split():
                            ldata.append( val )
                else:
                    if "==" in line:
                        dataline = True
        inp.close()
        return ldata


    def read_slab( self , dir , file ):
        ldata = []
        with open( dir+'/'+file , 'r' ) as inp:
            for i in range(7):
                next(inp)
            for line in inp:
                if "==" in line:
                    nextline = next(inp,None)
                    if nextline is not None:
                        if "AVERAGE" in nextline:
                            nextline = next(inp)
                        else:
                            for i in range(6):
                                next(inp)
                else:
                    for val in line.split():
                        ldata.append( val )
        inp.close()
        return ldata


# Returns the data in the file as a numpy array
    def read_data( self , dir , file ):
        type = self.get_file_type( file )
        data = []
        if self.verbose:
            print( "File" , dir + file , "is type" , type )
        if type == 'iter':
            data = np.array( self.read_iter( dir , file ) , dtype=np.double )
            self.iter = True
            self.ignore = True
        elif type == 'slab':
            data = np.array( self.read_slab( dir , file ) , dtype=np.double )
        elif type == 'binary':
            if not self.nobinary:
                data = np.fromfile(dir+'/'+file, dtype=np.double )
        return data


    
# Not used as it misses fort.10 for some reason
    def remove_binary_files( self , files ):
        for file in files:
            print( 'Checking' , file )
            try:
                with open( file , 'r' , encoding='utf-8') as f:
                    for line in f:
                        next(f)
                    f.close()
            except UnicodeDecodeError:
                files.remove( file )
        return files
                
    
    def get_files( self ):
# The remove binary files routine doesn't work...        
#        files1 = self.remove_binary_files( os.listdir(self.dir1) )        
#        files2 = self.remove_binary_files( os.listdir(self.dir2) )
        if args.file:            
            files1 = [args.file]
            files2 = [args.file]
        else:
            files1 = os.listdir(self.dir1) 
            files2 = os.listdir(self.dir2)
        if files1 == files2:
            return files1
        else:
            print("Directories contain different files - exiting")
            exit()                    

    def get_file_type( self , file ):
        if file in self.iter_files:
            return 'iter'
        elif file in self.slab_files:
            return 'slab'
        elif file in self.binary_files:
            return 'binary'
        else:
            return 'unknown'
                    
        
    def compare( self ):
        files = self.get_files()
        if self.verbose:
            print( "Comparing directory" , self.dir1 , "with", self.dir2 )
        for file in files:
            if not self.compare_file( file ):
                continue
            if not self.quiet:
                self.report_diff( file )
            self.reset()
        self.finish()

    
#####################################################################################
# Different output reading routines
#
#####################################################################################


#####################################################################################
# Read args
#
#####################################################################################
                        
def read_args():
    parser = argparse.ArgumentParser(description = 'Compare eirene output in two directories.')
    parser.add_argument( "-d" ,"--dir1" , default="output" , help='First directory containing output files (default=output)' )
    parser.add_argument( "-r" ,"--dir2" , default="output_ref" , help='Second directory containing output files (default=output_ref)' )
    parser.add_argument( "-f" ,"--file", default=None , help='If specified only this file will be checked' )
    parser.add_argument( "-q","--quiet" , action='store_const', const=True , default=False , help='Only prints a message if differences exist' )
    parser.add_argument( "-n","--nobinary" , action='store_const', const=False , default=True , help='Check binary files' )
    parser.add_argument( "-v","--verbose" , action='store_const', const=True , default=False , help='Prints more detail about the comparisons being made' )
    parser.add_argument( "-w","--warnings" , action='store_const', const=True , default=False , help='Prints out values of all mismatches above machine precision' )
    parser.add_argument( "-e","--errall" , action='store_const', const=True , default=False , help='Prints out all mismatches' )
    parser.add_argument( "-s","--single_precision" , action='store_const' , const=True , default=False , help='Sets tolerance to single precision' )
    parser.add_argument( "-i","--ignore_small_values" , action='store_const' , const=True , default=False , help='Ignore values that are much smaller than the mean' )
    parser.add_argument( "--ignore_factor" , type=float , default=1e-3 , help='Set the ratio to te mean at which small values are ignored' )
    args = parser.parse_args()
    check_args(args)
    return args

def check_args(args):
    if not os.path.isdir(args.dir1):
        print("Directory" , args.dir1 , "does not exist" )
        exit()
    if not os.path.isdir(args.dir2):
        print("Directory" , args.dir2 , "does not exist" )
        exit()
    if args.file is not None:
        if not os.path.exists(args.dir1+"/"+args.file):
            print("File" , args.file , "does not exist" )
            exit()
        if not os.path.exists(args.dir2+"/"+args.file):
            print("File" , args.file , "does not exist" )
            exit()
    if args.errall:
        args.warnings = True
    if args.verbose:
        if args.single_precision:
            print( "Comparing to single precision" )
            
#################################################################################################
# Main
#################################################################################################

args = read_args()

# this was stopping on a floating point error in mean
#np.seterr('raise')
np.set_printoptions(threshold=sys.maxsize)

c = comparison( args )
c.compare()

if args.verbose:
    print( "Finished comparing eirene output" )

sys.exit(c.err)
